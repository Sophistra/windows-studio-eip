﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ws_web_app.Models;

namespace ws_web_app.Controllers
{
    public class CreateProjectController : Controller
    {
        // GET: CreateProject
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ProjectModel model)
        {
            if (ModelState.IsValid)
            {
                // Vérification si le login et le password correspondent
                if (model.Name != null && model.Description != null)
                {
                    // Si reussi, faire quelque chose
                    Models.DataAccessLayer.AddNewProject(model.Name, model.Description);
                }

            }
            return RedirectToAction("Index", "Projet");
        }
    }
}