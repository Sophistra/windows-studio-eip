﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace ws_web_app.Models
{
    public class DataAccessLayer
    {
        static SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDbConnectionString"].ToString());

        public static List<Project> GetAllProjects()
        {
            List<Project> projects = new List<Project>();
            string query = string.Format("SELECT * FROM [ws_project]");

            List<User> users = GetAllUsers();
            int id;

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                Project project = new Project();
                project.name = sdr.GetString(1);
                project.description = sdr.GetString(2);
                project.date = sdr.GetDateTime(3);
                id = sdr.GetInt32(4) - 1;
                project.user = users[id].name;
                projects.Add(project);
            }

            conn.Close();
            return (projects);
        }

        public static List<User> GetAllUsers()
        {
            List<User> users = new List<User>();
            string query = string.Format("SELECT * FROM [ws_user]");

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                User user = new User();
                user.name = sdr.GetString(1) + " " + sdr.GetString(2);
                user.id = sdr.GetInt32(0);
                users.Add(user);
            }

            conn.Close();
            return (users);
        }

        public static bool AddNewProject(string name, string description)
        {
            DateTime localDate = DateTime.Now;
            var sqlFormattedDate = localDate.ToString("yyyy-MM-dd HH:mm:ss");
            string query = string.Format("INSERT INTO [ws_project] ([project_name], [project_desc], [project_date_creation], [project_id_user]) VALUES('{0}', '{1}', '{2}', '{3}')", name, description, sqlFormattedDate, 1);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            conn.Close();
            return (true);
        }

        public static List<Review> GetAllReviews()
        {
            List<Review> reviews = new List<Review>();
            string query = string.Format("SELECT * FROM [ws_review]");

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                Review review = new Review();
                review.username = sdr.GetString(1);
                review.message = sdr.GetString(2);
                review.date = sdr.GetDateTime(3);
                reviews.Add(review);
            }

            conn.Close();
            return (reviews);
        }

        public static bool AddNewReview(string username, string message)
        {
            DateTime localDate = DateTime.Now;
            var sqlFormattedDate = localDate.ToString("yyyy-MM-dd HH:mm:ss");
            string query = string.Format("INSERT INTO [ws_review] ([review_username], [review_msg], [review_date_creation]) VALUES('{0}', '{1}', '{2}')", username, message, sqlFormattedDate);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            conn.Close();
            return (true);
        }
    }
}