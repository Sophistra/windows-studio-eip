﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ws_web_app.Models
{
    public class Project
    {
        public string name { get; set; }
        public string description { get; set; }
        public DateTime date { get; set; }
        public string user { get; set; }
    }
}