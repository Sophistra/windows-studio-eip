﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ws_web_app.Models
{
    public class Review
    {
        public string username { get; set; }
        public string message { get; set; }
        public DateTime date { get; set; }
    }
}