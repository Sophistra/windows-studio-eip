﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ws_web_app.Models
{
    public class ProjectModel
    {
            [Required]
            [Display(Name = "Nom du projet")]
            public string Name { get; set; }

            [Required]
            [Display(Name = "Description du projet")]
            public string Description { get; set; }
    }
}